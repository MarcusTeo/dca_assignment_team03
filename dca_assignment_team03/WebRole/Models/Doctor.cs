﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebRole.Models
{
    public class Doctor
    {
        public Doctor() { }
        public int DoctorID { get; set; }
        public string NRIC { get; set; }
        public string Name { get; set; }

        public int PhoneNo { get; set; }

        [Display(Name = "Gender")]
        [StringLength(1, ErrorMessage = "Please enter either F or M only")]
        public char gender { get; set; }
        public DateTime dob { get; set; }
        public string Speciality { get; set; }

        [Display(Name = "Email")]
        [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}")]
        public string EmailAddr { get; set; }
    }
}
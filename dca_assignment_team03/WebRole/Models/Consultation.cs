﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace WebRole.Models
{
    public class Consultation
    {
        public Consultation() { }
        public int ConsultationID {get;set;}
        
        public string Reason { get; set; }
        public DateTime DateTime { get; set; }
        
        public int PatientID { get; set; }
        public int DoctorID { get; set; }
        public string Status { get; set; }
        
        public string ConsoultationMode { get; set; }
    }
}